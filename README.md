## node map
- Web crawler
    - source
        - stackoverflow
    - framework
        - scrapy
            - spider
            - item
            - middleware
                - proxy
                - ua
            - pipeline
            - setting
    - db
        - mysql
        - redis
    - export
        - csv
    - deploy
        - aws
    - algorithm
        - bloom filter

## docs
1. https://docs.scrapy.org/en/latest/topics/api.html  scrapy doc
2. https://scrapyd.readthedocs.io/en/latest/api.html  scrapyd doc
3. https://redis-py.readthedocs.io/en/latest/         redis-py doc

## tuts
1. https://piaosanlang.gitbooks.io/spiders/content/    web crawling gitbook

## create a scrapy project
1. scrapy startproject scrapy_stackoverflow

## run a spider
1. scrapy crawl stackoverflow -a tag=java -o stackoverflow.json
2. scrapy crawl stackoverflow -a tag=java
3. scrapy crawl stackoverflow -a tag=java -s JOBDIR=crawls/stackoverflow-spider-1

## scrapy shell
1. the result is a list

```
>>> response.css('title::text').extract()
['Quotes to Scrape']
```

2. the result is a string

```
>>> response.css('title::text')[0].extract()
'Quotes to Scrape'

>>> response.xpath('//title/text()')[0].extract()
'Quotes to Scrape'
```

## run contract checks
1. scrapy check [-l] <spider>

## run a single spider
1. scrapy runspider <spider_file.py>

## selector

```
from scrapy import selector
html_selector = selector.Selector(response=response, type="html")
```

## scrapyd
1. pip3 install scrapyd
2. pip3 install scrapyd-client
3. pip3 install scrapy-redis
4. scrapyd
5. scrapyd-deploy -l
6. scrapyd-deploy local-target -p crawl_stackoverflow
7. curl http://localhost:6800/schedule.json -d project=crawl_stackoverflow -d spider=stackoverflow
8. curl http://localhost:6800/cancel.json -d project=crawl_stackoverflow -d job=<job-id>
9. curl http://localhost:6800/listspiders.json?project=crawl_stackoverflow
10. curl http://localhost:6800/delproject.json -d project=crawl_stackoverflow

## todo
1. mail not working?
2. resume to crawling?
3. aws deploy
4. spider middleware
5. -o output is not working?
6. nginx?
7. Fiddler?
8. bloomfilter...stop crawling...        - solved. It's the logic of the spider not good enough.
9. scheduling a lot of spiders
