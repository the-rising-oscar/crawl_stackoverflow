import scrapy
from scrapy import selector
from crawl_stackoverflow.items import CrawlStackoverflowItem
import requests
import hashlib


def url_hash(url):
    return hashlib.md5(url.encode('utf-8')).hexdigest()


def getpages(url):
    r = requests.get(url)
    current_page = selector.Selector(text=r.text, type="html")
    pages = current_page.xpath(
        '//div[@class="pager fl"]/a[position()=last()-1]/span/text()').extract_first()
    return int(pages) + 1


class StackoverflowSpider(scrapy.Spider):
    name = "stackoverflow"
    base_url = 'https://stackoverflow.com'
    user_agents_enabled = True
    proxies_enabled = True

    def start_requests(self):
        untagged_questions = 'https://stackoverflow.com/questions?page={}&sort=active&size=50'
        tagged_questions = 'https://stackoverflow.com/questions/tagged/{}?page={}&sort=active&pagesize=50'
        tag = getattr(self, 'tag', None)
        if tag is None:
            urls = [untagged_questions.format(n) for n in range(
                1, getpages(untagged_questions.format(1)))]
            for u in urls:
                request = scrapy.Request(
                    url=u, callback=self.questions_page)
                request.meta['tag'] = 'untagged questions'
                yield request
        else:
            urls = [tagged_questions.format(str(tag), n)
                    for n in range(1, getpages(tagged_questions.format(str(tag), 1)))]
            for u in urls:
                request = scrapy.Request(
                    url=u, callback=self.questions_page)
                request.meta['tag'] = str(tag)
                yield request

    def questions_page(self, response):
        current_page = selector.Selector(response=response, type="html")
        q_block = current_page.xpath('//div[@id="questions"]')
        q_links = q_block.xpath(
            '//a[@class="question-hyperlink"]/@href').extract()
        q_links = [q for q in q_links if 'https://' not in q]
        for q_link in q_links:
            q_link = ''.join([self.base_url, q_link])
            request = scrapy.Request(url=q_link, callback=self.question_page)
            request.meta['tag'] = response.meta['tag']
            yield request

    def question_page(self, response):
        current_page = selector.Selector(response=response, type="html")
        item = CrawlStackoverflowItem()
        item['question_body'] = current_page.xpath(
            '//div[contains(@class, "postcell")]/div[@class="post-text"]').extract_first()
        item['question_title'] = current_page.xpath(
            '//h1/a[@class="question-hyperlink"]/text()').extract_first()
        item['accepted_answer'] = current_page.xpath(
            '//div[contains(@class, "accepted-answer")]//div[@class="post-text"]').extract_first()
        item['other_answers'] = ''.join(current_page.xpath(
            '//div[@class="answer"]//div[@class="post-text"]').extract())
        item['question_url_hash'] = url_hash(response.url)
        item['tag'] = response.meta['tag']

        return item
