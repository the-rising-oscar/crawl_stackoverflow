# -*- coding: utf-8 -*-


BOT_NAME = 'crawl_stackoverflow'

SPIDER_MODULES = ['crawl_stackoverflow.spiders']
NEWSPIDER_MODULE = 'crawl_stackoverflow.spiders'


USER_AGENTS = [
    'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:23.0) Gecko/20100101 Firefox/23.0',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.62 Safari/537.36',
    'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64; rv:24.0) Gecko/20140205 Firefox/24.0 Iceweasel/24.3.0',
    'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:28.0) Gecko/20100101 Firefox/28.0',
    'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:28.0) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2',
]

PROXIES = [
    'http://144.217.204.254:3128',
    'http://121.129.127.209:80',
    'http://118.193.26.18:8080',
    'http://47.52.153.167:443',
    'http://91.215.176.27:81',
]

# Haha
ROBOTSTXT_OBEY = False

DOWNLOAD_DELAY = 3

DOWNLOADER_MIDDLEWARES = {
    'crawl_stackoverflow.middlewares.RotateUserAgentMiddleware': 100,
    'crawl_stackoverflow.middlewares.RotateProxyMiddleware': 99,
}


ITEM_PIPELINES = {
    'crawl_stackoverflow.pipelines.CrawlStackoverflowPipelineToMysql': 300,
    # 'crawl_stackoverflow.bloomfilter_pipelines.CrawlStackoverflowBloomfilterPipelineToMysql': 300,
    # 'crawl_stackoverflow.exporters.PerTagCsvExportPipeline': 300,
}


LOG_ENABLED = True
LOG_ENCODING = 'utf-8'
LOG_FILE = 'custom-logs/scrapy.log'
LOG_LEVEL = 'DEBUG'
LOG_STDOUT = True
