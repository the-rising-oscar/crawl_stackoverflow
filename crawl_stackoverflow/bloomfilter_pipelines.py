# -*- coding: utf-8 -*-
import pymysql
import redis
from .bloomfilter import BloomFilter
import scrapy


class CrawlStackoverflowBloomfilterPipelineToMysql(object):
    def open_spider(self, spider):
        self.connection = pymysql.connect(host='localhost',
                                          user='root',
                                          password='uuid9',
                                          db='bf_crawl_stackoverflow',
                                          charset='utf8mb4',
                                          cursorclass=pymysql.cursors.Cursor)

        self.bloom_filter = BloomFilter()

    def close_spider(self, spider):
        self.connection.close()

    def process_item(self, item, spider):
        if self.bloom_filter.is_contains(item['question_url_hash']):
            raise scrapy.exceptions.DropItem(
                "Bloomfilter: Duplicate item found -> ")
        else:
            with self.connection.cursor() as cursor:
                sql = "INSERT INTO `crawl_stackoverflow_collection` (`question_title`, `question_url_hash`, `question_body`, `accepted_answer`, `other_answers`, `tag`) VALUES (%s, %s, %s, %s, %s, %s)"
                cursor.execute(sql, (item['question_title'], item['question_url_hash'],
                                     item['question_body'], item['accepted_answer'], item['other_answers'], item['tag']))
                self.connection.commit()
                self.bloom_filter.insert(item['question_url_hash'])
