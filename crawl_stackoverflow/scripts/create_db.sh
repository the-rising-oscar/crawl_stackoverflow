#!/bin/bash

echo "Usage:"
echo "  $ sudo chmod a+x create_db.sh"
echo "  $ ./create_db.sh"


mysql_up=$(pgrep mysql | wc -l);
if [ "$mysql_up" -ne 1 ]
then
  echo "mysql is down."
  exit 0
fi


db="crawl_stackoverflow"
table="crawl_stackoverflow_collection"
mysql -u root -puuid9 <<EOF
set names 'utf8mb4';
create database if not exists $db;
use $db;
create table if not exists $table(
  id int unsigned not null auto_increment,
  question_title varchar(300) null,
  question_body text null,
  question_url_hash varchar(500) null,
  accepted_answer text null,
  other_answers text null,
  tag varchar(30) null,
  primary key(id)
);
EOF

echo "db $db is up."
echo "table $table is up."
