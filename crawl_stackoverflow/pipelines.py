# -*- coding: utf-8 -*-
import pymysql
import redis
import pandas as pd
from scrapy import exceptions


class CrawlStackoverflowPipelineToMysql(object):
    def open_spider(self, spider):
        self.connection = pymysql.connect(host='localhost',
                                          user='root',
                                          password='uuid9',
                                          db='crawl_stackoverflow',
                                          charset='utf8mb4',
                                          cursorclass=pymysql.cursors.Cursor)

        self.redis_connection = redis.StrictRedis(
            host='localhost', db=0)
        self.redis_connection.flushdb()
        self.redis_hash = 'url_existed'
        if self.redis_connection.hlen(self.redis_hash) == 0:
            sql = "SELECT question_url_hash FROM crawl_stackoverflow_collection;"
            df = pd.read_sql(sql, self.connection)
            for key in df['question_url_hash'].get_values():
                self.redis_connection.hset(self.redis_hash, key, 0)

    def close_spider(self, spider):
        self.connection.close()

    def process_item(self, item, spider):
        if self.redis_connection.hexists(self.redis_hash, item['question_url_hash']):
            raise exceptions.DropItem("Duplicate found!")

        with self.connection.cursor() as cursor:
            sql = "INSERT INTO crawl_stackoverflow_collection (question_title, question_url_hash, question_body, accepted_answer, other_answers, tag) VALUES (%s, %s, %s, %s, %s, %s)"
            cursor.execute(sql, (item['question_title'], item['question_url_hash'],
                                 item['question_body'], item['accepted_answer'], item['other_answers'], item['tag']))

        self.connection.commit()
        print('Added')
        return item
