# -*- coding: utf-8 -*-

import random
from scrapy import exceptions

# Downloader Middlewares
#


class RotateUserAgentMiddleware(object):

    def __init__(self, user_agents):
        self.enabled = False
        self.user_agents = user_agents

    @classmethod
    def from_crawler(cls, crawler):
        user_agents = crawler.settings.get('USER_AGENTS', [])
        if not user_agents:
            raise exceptions.NotConfigured('USER_AGENTS not set or empty')
        s = cls(user_agents)
        return s

    def process_request(self, request, spider):
        # - return None: continue processing this request
        # - return a Response object
        # - return a Request object
        # - raise IgnoreRequest

        if not self.enabled or not self.user_agents:
            return None

        request.headers['user-agent'] = random.choice(self.user_agents)
        return request

    def process_response(self, request, response, spider):
        return response

    def process_exception(self, request, exception, spider):
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        return None

    def spider_opened(self, spider):
        self.enabled = getattr(spider, 'user_agents_enabled', self.enabled)


class RotateProxyMiddleware(object):

    def __init__(self, proxies):
        self.enabled = False
        self.proxies = proxies

    @classmethod
    def from_crawler(cls, crawler):
        proxies = crawler.settings.get('PROXIES', [])
        if not proxies:
            raise exceptions.NotConfigured('PROXIES not set or empty')
        s = cls(proxies)
        return s

    def process_request(self, request, spider):
        if not self.enabled or not self.proxies:
            return None

        request.meta['proxy'] = random.choice(self.proxies)
        return request

    def process_response(self, request, response, spider):
        return response

    def process_exception(self, request, exception, spider):
        return None

    def spider_opened(self, spider):
        self.enabled = getattr(spider, 'proxies_enabled', self.enabled)
