from scrapy import exporters
import os
import shutil
import logging

logger = logging.getLogger('stackoverflow')

class PerTagCsvExportPipeline(object):
    """Distribute items across multiple CSV files according to their 'tag' field"""

    def open_spider(self, spider):
        self.tag_to_exporter = {}
        self.folder = 'crawl_stackoverflow/csv-exports'
        if not os.path.exists(self.folder):
            os.makedirs(self.folder)

    def close_spider(self, spider):
        for exporter in self.tag_to_exporter.values():
            exporter.finish_exporting()
            exporter.file.close()

    def _exporter_for_item(self, item):
        tag = item['tag']
        if tag not in self.tag_to_exporter:
            filename = "{}/{}.csv".format(self.folder, tag)
            if os.path.isfile(filename):
                os.remove(filename)
            f = open(filename, 'wb')
            exporter = exporters.CsvItemExporter(f)
            exporter.start_exporting()
            self.tag_to_exporter[tag] = exporter
        return self.tag_to_exporter[tag]

    def process_item(self, item, spider):
        exporter = self._exporter_for_item(item)
        exporter.export_item(item)
        logger.warning('Exported: To csv format:\n%s', str({'question_title': item['question_title']}))
