# -*- coding: utf-8 -*-
import scrapy

class CrawlStackoverflowItem(scrapy.Item):
    question_title = scrapy.Field()
    question_body = scrapy.Field()
    accepted_answer = scrapy.Field()
    other_answers = scrapy.Field()
    question_url_hash = scrapy.Field()
    tag = scrapy.Field()

    def __repr__(self):
        return repr({'question_title': self['question_title']})
