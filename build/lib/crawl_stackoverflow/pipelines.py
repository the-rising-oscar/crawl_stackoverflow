# -*- coding: utf-8 -*-
import pymysql
from .credentials import credentials_mysql, credentials_redis, credentials_mail_to
import redis
import pandas as pd
import hashlib
import scrapy
import logging
from scrapy import mail, conf

logger = logging.getLogger('stackoverflow')

class CrawlStackoverflowPipelineToMysql(object):
    def open_spider(self, spider):
        self.connection = pymysql.connect(host=credentials_mysql['host'],
                                     user=credentials_mysql['user'],
                                     password=credentials_mysql['passwd'],
                                     db='crawl_stackoverflow',
                                     charset='utf8mb4',
                                     cursorclass=pymysql.cursors.Cursor)

        self.redis_connection = redis.StrictRedis(host=credentials_redis['host'], db=0)
        self.redis_connection.flushdb()
        self.redis_hash = 'url_existed'
        if self.redis_connection.hlen(self.redis_hash) == 0:
            sql = "SELECT question_url_hash FROM crawl_stackoverflow_collection;"
            df = pd.read_sql(sql, self.connection)
            for key in df['question_url_hash'].get_values():
                self.redis_connection.hset(self.redis_hash, key, 0)

        self.mailer = mail.MailSender.from_settings(conf.settings)
        self.mailto_list = [credentials_mail_to['author_email']]


    def close_spider(self, spider):
        self.connection.close()
        for email in self.mailto_list:
            with open('./mail-templates/after-run.txt') as f:
                mail_content = f.read().format(email, spider.crawler.stats.get_stats())
            f.close()
            self.mailer.send(to=[email], subject="Spider finished", body=mail_content, cc=[])



    def process_item(self, item, spider):
        if self.redis_connection.hexists(self.redis_hash, item['question_url_hash']):
            raise scrapy.exceptions.DropItem("Duplicate item found:")
        else:
            with self.connection.cursor() as cursor:
                sql = "INSERT INTO `crawl_stackoverflow_collection` (`question_title`, `question_url_hash`, `question_body`, `accepted_answer`, `other_answers`, `tag`) VALUES (%s, %s, %s, %s, %s, %s)"
                cursor.execute(sql, (item['question_title'], item['question_url_hash'], item['question_body'], item['accepted_answer'], item['other_answers'], item['tag']))
                self.connection.commit()
                logger.warning('Added: New item found:\n%s', str({'question_title': item['question_title']}))
                print({"Added question_title": item['question_title']})
