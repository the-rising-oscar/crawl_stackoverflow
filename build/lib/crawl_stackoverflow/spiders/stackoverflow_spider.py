import scrapy
from scrapy import selector
from crawl_stackoverflow.items import CrawlStackoverflowItem
import logging

def html_prettifier(unprettified):
    from bs4 import BeautifulSoup as bs
    if isinstance(unprettified, str):
        soup = bs(unprettified, "lxml")
        return soup.prettify()
    elif isinstance(unprettified, list):
        for s in unprettified:
            soup = bs(s, "lxml")
            soup.prettify()
        return unprettified

def url_hash(url_unhashed):
    import hashlib
    return hashlib.md5(url_unhashed.encode('utf-8')).hexdigest()

def str_join(l):
    return ''.join(l)

logger = logging.getLogger('stackoverflow')

class StackoverflowSpider(scrapy.Spider):
    name = "stackoverflow"
    base_url = 'https://stackoverflow.com'
    # allowed_domains = []
    # custom_settings = {}

    def start_requests(self):
        all_questions = 'https://stackoverflow.com/questions'
        tagged_questions = 'https://stackoverflow.com/questions/tagged/'
        tag = getattr(self, 'tag', None)
        if tag is None:
            request = scrapy.Request(url=all_questions, callback=self.questions_page)
            request.meta['tag'] = 'all questions'
            yield request
        else:
            tagged_questions = tagged_questions + str(tag)
            request = scrapy.Request(url=tagged_questions, callback=self.questions_page)
            request.meta['tag'] = str(tag)
            yield request

    def questions_page(self, response):
        current_page = selector.Selector(response=response, type="html")
        q_block = current_page.xpath('//div[@id="questions"]')
        q_links = q_block.xpath('//a[@class="question-hyperlink"]/@href').extract()
        q_links = [q for q in q_links if 'https://' not in q]
        for q_link in q_links:
            q_link = self.base_url + q_link
            request = scrapy.Request(url=q_link, callback=self.question_page)
            request.meta['tag'] = response.meta['tag']
            yield request

    def question_page(self, response):
        logger.info('Collecting data on `%s`', response.url)
        current_page = selector.Selector(response=response, type="html")
        item = CrawlStackoverflowItem()

        question_body = current_page.xpath('//div[@class="post-text"]').extract()
        item['question_body'] = html_prettifier(str_join(question_body))

        question_title = current_page.xpath('//h1/a[@class="question-hyperlink"]/text()').extract()
        item['question_title'] = str_join(question_title)

        accepted_answer = current_page.xpath('//div[@class="answer accepted-answer"]//div[@class="post-text"]//p').extract()
        item['accepted_answer'] = html_prettifier(str_join(accepted_answer))

        other_answers = current_page.xpath('//div[@class="answer"]//div[@class="post-text"]//p').extract()
        item['other_answers'] = html_prettifier(str_join(other_answers))

        item['question_url_hash'] = url_hash(response.url)
        item['tag'] = response.meta['tag']

        return item
