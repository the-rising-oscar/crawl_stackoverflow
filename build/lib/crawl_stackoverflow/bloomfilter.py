# coding: utf-8

import mmh3
import redis
from .credentials import credentials_redis


class BloomFilter(object):

    def __init__(self):
        self.redis_connection = redis.StrictRedis(host=credentials_redis['host'], db=1)
        self.seeds = [7, 11, 29, 51, 57, 67, 73]
        self.bitarray_size = (1 << 31) - 1 
        self.key = 'bloomfilter-stackoverflow'

    def get_offsets(self, url):
        return [mmh3.hash(url, seed) % self.bitarray_size for seed in self.seeds]

    def is_contains(self, url):
        if not url:
            return False
        locs = self.get_offsets(url)

        return all([True if self.redis_connection.getbit(self.key, loc) else False for loc in locs])

    def insert(self, url):
        locs = self.get_offsets(url)
        for loc in locs:
            self.redis_connection.setbit(self.key, loc, 1)

if __name__ == '__main__':
    bloom_filter = BloomFilter()
    print(bloom_filter.is_contains('url'))
    bloom_filter.insert('url')
    print(bloom_filter.is_contains('url'))
